FROM python:3.9-alpine

WORKDIR /app

COPY app.py requirements.txt ./

RUN pip install -r requirements.txt

ENTRYPOINT ["python", "app.py" ]

EXPOSE 8080