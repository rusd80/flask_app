"""This module shows a simple web page"""

from flask import Flask

app = Flask(__name__)


@app.route('/', methods=['GET'])
def index():
    """
    function returns message that shown on web page
    :return: message
    """
    return "Hello World 123"


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
