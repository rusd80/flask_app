"""
Test for app checks 'Hello world' output
"""
from app import app


def test_get():
    """
    GIVEN a Flask application
    WHEN the '/' page is requested (GET)
    THEN check that the response is valid
    """
    with app.test_client() as client:
        response = client.get('/')
        assert response.status_code == 200
