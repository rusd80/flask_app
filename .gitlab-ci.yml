stages:
  - test
  - build
  - scan
  - deploy

flake8:
  stage: test
  image: python:3.9-buster
  before_script:
    - pip install flake8
  script:
    - flake8 *.py
  after_script:
    - sh ci-notify.sh $CI_JOB_STATUS

pylint:
  stage: test
  image: python:3.9-buster
  before_script:
    - pip install pylint -r requirements.txt
  script:
    - pylint *.py
  after_script:
    - sh ci-notify.sh $CI_JOB_STATUS

pytest:
  stage: test
  image: python:3.9-buster
  before_script:
    - pip install pytest -r requirements.txt
  script:
    - pytest test.py --junitxml=report.xml
  artifacts:
    when: always
    reports:
      junit: report.xml
  after_script:
    - sh ci-notify.sh $CI_JOB_STATUS

sonarqube-check:
  stage: test
  image:
    name: sonarsource/sonar-scanner-cli:4.6
    entrypoint: [""]
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner
  allow_failure: true
  only:
    - master
  after_script:
    - sh ci-notify.sh $CI_JOB_STATUS

build-container:
  needs:
    - flake8
    - pylint
    - pytest
    - sonarqube-check
  image: docker:19.03.12
  stage: build
  services:
    - docker:19.03.12-dind
  before_script:
    - apk update
    - apk add curl
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t registry.gitlab.com/rusd80/flask_app:$CI_COMMIT_SHORT_SHA .
    - docker push registry.gitlab.com/rusd80/flask_app:$CI_COMMIT_SHORT_SHA
  after_script:
    - sh ci-notify.sh $CI_JOB_STATUS

container-scan:
  needs:
    - build-container
  image: registry.gitlab.com/security-products/container-scanning:4
  stage: scan
  variables:
    GIT_STRATEGY: none
    DOCKER_IMAGE: registry.gitlab.com/rusd80/flask_app:$CI_COMMIT_SHORT_SHA
  allow_failure: true
  artifacts:
    reports:
      container_scanning: gl-container-scanning-report.json
      dependency_scanning: gl-dependency-scanning-report.json
    paths: [gl-container-scanning-report.json, gl-dependency-scanning-report.json]
  dependencies: []
  script:
    - gtcs scan

deploy_app:
  needs:
    - container-scan
  stage: deploy
  image:
    name: bitnami/kubectl:1.21.8
    entrypoint: [""]
  script:
    - sed -i "s|latest|${CI_COMMIT_SHORT_SHA}|" deployment.yaml
    - kubectl config use-context rusd80/flask_app:primary-agent-fl
    - kubectl get pod -A
    - kubectl apply -f deployment.yaml
    - kubectl apply -f loadbalancer.yaml
    - sleep 5
    - echo http://$(kubectl get service | grep LoadBalancer | awk '{print $4}')
  after_script:
    - sh ci-notify.sh $CI_JOB_STATUS
