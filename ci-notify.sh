#!/bin/bash

TIME="10"
URL="https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage"
TEXT="Job status: $1%0A%0AProject:+$CI_PROJECT_NAME%0AJob:+$CI_JOB_NAME \
      %0AURL:+$CI_PROJECT_URL%0AStage:+$CI_JOB_STAGE%0AStarted:+$CI_JOB_STARTED_AT"

curl -s --max-time $TIME -d "chat_id=$TELEGRAM_USER_ID&disable_web_page_preview=1&text=$TEXT" $URL > /dev/null

