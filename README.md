

















## Andersen Devops exam

#### This is CI/CD pipeline for "hello world" Flask application.
Pipeline has 4 stages:

- test
- build
- scan
- deploy

![](/home/cube366/flask_app/flask_pipe.png)

### Test
Uses:
- Flake 8
- Pylint
- Pytest (unit test)
- Sonarqube

### Build
Creating docker container and pushing to gitlab registry.

### Scan
Scanning container with Trivy for vulnerabilities

### Deploy
Deploying container to AWS Elastic Kubernetes Services.
